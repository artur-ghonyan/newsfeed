package com.newsfeed.newsfeed.application;

import android.app.Application;
import com.newsfeed.newsfeed.utils.PrefManager;

public class MyApplication extends Application {
    private static boolean activityVisible;

    @Override
    public void onCreate() {
        super.onCreate();
        PrefManager.init(this);
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }
}
