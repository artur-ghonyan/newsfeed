package com.newsfeed.newsfeed.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;
import com.newsfeed.newsfeed.model.Results;

import static com.newsfeed.newsfeed.utils.Constants.NewsInfo.*;

public class NewsDBHelper extends SQLiteOpenHelper {
    private final Context context;

    private static final String DATABASE_NAME = "NEWSFEED.DB";
    private static final int DATABASE_VERSION = 1;
    private static final String CREATE_QUERY =
            "CREATE TABLE " +
                    TABLE_NAME +
                    "(" +
                    NEWS_ID + " TEXT PRIMARY KEY," +
                    NEWS_TITLE + " TEXT," +
                    NEWS_IMG_URL + " TEXT," +
                    NEWS_CATEGORY + " TEXT," +
                    NEWS_SECTION_ID + " TEXT," +
                    NEWS_SECTION_NAME + " TEXT," +
                    NEWS_WEB_PUBLICATION_DATE + " TEXT," +
                    NEWS_WEB_URL + " TEXT," +
                    NEWS_API_URL + " TEXT," +
                    NEWS_FIELDS_HEAD_LINE + " TEXT," +
                    NEWS_FIELDS_STAR_RATING + " TEXT," +
                    NEWS_FIELDS_SHORT_URL + " TEXT," +
                    NEWS_IS_HOSTED + " boolean," +
                    NEWS_PILLAR_ID + " TEXT," +
                    NEWS_PILLAR_NAME + " TEXT," +
                    NEWS_LIST + " boolean," +
                    PIN_LIST + " boolean" +
                    ");";


    public NewsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void addNews(Results results, SQLiteDatabase db) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NEWS_ID, results.getId());
        contentValues.put(NEWS_TITLE, results.getWebTitle());
        contentValues.put(NEWS_IMG_URL, results.getFields().getThumbnail());
        contentValues.put(NEWS_CATEGORY, results.getType());
        contentValues.put(NEWS_SECTION_ID, results.getSectionId());
        contentValues.put(NEWS_SECTION_NAME, results.getSectionName());
        contentValues.put(NEWS_WEB_PUBLICATION_DATE, results.getWebPublicationDate());
        contentValues.put(NEWS_WEB_URL, results.getWebUrl());
        contentValues.put(NEWS_API_URL, results.getApiUrl());
        contentValues.put(NEWS_FIELDS_HEAD_LINE, results.getFields().getHeadline());
        contentValues.put(NEWS_FIELDS_STAR_RATING, results.getFields().getStarRating());
        contentValues.put(NEWS_FIELDS_SHORT_URL, results.getFields().getShortUrl());
        contentValues.put(NEWS_IS_HOSTED, results.getHosted());
        contentValues.put(NEWS_PILLAR_ID, results.getPillarId());
        contentValues.put(NEWS_PILLAR_NAME, results.getPillarName());
        contentValues.put(NEWS_LIST, results.isNewsList());
        contentValues.put(PIN_LIST, results.isPinList());
        db.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor readInformation(SQLiteDatabase db) {
        Cursor cursor;
        String[] columns = {NEWS_ID, NEWS_TITLE, NEWS_IMG_URL, NEWS_CATEGORY, NEWS_SECTION_ID,
                NEWS_SECTION_NAME, NEWS_WEB_PUBLICATION_DATE, NEWS_WEB_URL, NEWS_API_URL,
                NEWS_FIELDS_HEAD_LINE, NEWS_FIELDS_STAR_RATING, NEWS_FIELDS_SHORT_URL,
                NEWS_IS_HOSTED, NEWS_PILLAR_ID, NEWS_PILLAR_NAME, NEWS_LIST, PIN_LIST};
        cursor = db.query(TABLE_NAME, columns, null, null, null, null, null);
        return cursor;
    }

    public void deleteNews(String id, SQLiteDatabase db) {
        String selection = NEWS_ID + " LIKE ?";
        String[] selection_args = {id};
        db.delete(TABLE_NAME, selection, selection_args);
        Toast.makeText(context, "news was deleted", Toast.LENGTH_SHORT).show();
    }

    public Cursor searchNews(String id, SQLiteDatabase db) {
        String selection = NEWS_ID + " LIKE ?";
        String[] selection_args = {id};
        String[] columns = {NEWS_LIST, PIN_LIST};
        return db.query(TABLE_NAME, columns, selection, selection_args, null, null, null);
    }

    public void updateNews(Results results, SQLiteDatabase db) {
        String selection = NEWS_ID + " LIKE ?";
        String[] selection_args = {results.getId()};

        ContentValues contentValues = new ContentValues();
        contentValues.put(NEWS_ID, results.getId());
        contentValues.put(NEWS_TITLE, results.getWebTitle());
        contentValues.put(NEWS_IMG_URL, results.getFields().getThumbnail());
        contentValues.put(NEWS_CATEGORY, results.getType());
        contentValues.put(NEWS_SECTION_ID, results.getSectionId());
        contentValues.put(NEWS_SECTION_NAME, results.getSectionName());
        contentValues.put(NEWS_WEB_PUBLICATION_DATE, results.getWebPublicationDate());
        contentValues.put(NEWS_WEB_URL, results.getWebUrl());
        contentValues.put(NEWS_API_URL, results.getApiUrl());
        contentValues.put(NEWS_FIELDS_HEAD_LINE, results.getFields().getHeadline());
        contentValues.put(NEWS_FIELDS_STAR_RATING, results.getFields().getStarRating());
        contentValues.put(NEWS_FIELDS_SHORT_URL, results.getFields().getShortUrl());
        contentValues.put(NEWS_IS_HOSTED, results.getHosted());
        contentValues.put(NEWS_PILLAR_ID, results.getPillarId());
        contentValues.put(NEWS_PILLAR_NAME, results.getPillarName());
        contentValues.put(NEWS_LIST, results.isNewsList());
        contentValues.put(PIN_LIST, results.isPinList());
        db.update(TABLE_NAME, contentValues, selection, selection_args);
    }
}
