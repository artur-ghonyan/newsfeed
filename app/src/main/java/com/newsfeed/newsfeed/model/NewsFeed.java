package com.newsfeed.newsfeed.model;

public class NewsFeed {
    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}