package com.newsfeed.newsfeed.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.newsfeed.newsfeed.R;
import com.newsfeed.newsfeed.interfaces.OnRecyclerViewItemClickListener;
import com.newsfeed.newsfeed.model.Results;
import com.newsfeed.newsfeed.utils.NewsFeedManager;
import com.newsfeed.newsfeed.utils.PrefManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.newsfeed.newsfeed.utils.Constants.MenuItems.LINEAR_LAYOUT_MANAGER;
import static com.newsfeed.newsfeed.utils.Constants.MenuItems.SAVED_LIST;
import static com.newsfeed.newsfeed.utils.Constants.NewsInfo.NEWS_LIST;
import static com.newsfeed.newsfeed.utils.Constants.NewsInfo.PIN_LIST;

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.MyViewHolder> {
    private List<Results> data;
    private final OnRecyclerViewItemClickListener onClickListener;
    private final LayoutInflater mInflater;
    private final Context mContext;

    public NewsFeedAdapter(Context context, List<Results> data, OnRecyclerViewItemClickListener onClickListener) {
        mContext = context;
        this.data = data;
        this.onClickListener = onClickListener;
        this.mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        if (viewType == 0) {
            view = mInflater.inflate(R.layout.news_feed_item_linear, viewGroup, false);
        } else {
            view = mInflater.inflate(R.layout.news_feed_item_grid, viewGroup, false);
        }
        return new NewsFeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.bind(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (PrefManager.getInstance().getLayoutManagerType() == LINEAR_LAYOUT_MANAGER) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<Results> getData() {
        return data;
    }

    public void setData(List<Results> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public abstract class MyViewHolder extends RecyclerView.ViewHolder {

        private MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        protected abstract void bind(int position);
    }

    protected class NewsFeedViewHolder extends MyViewHolder {
        private final TextView labelTitle;
        private final TextView labelCategory;
        private final ImageView imageView;
        private final ImageView saveDeleteNews;
        private final ImageView saveInPinList;
        private final ViewGroup containerView;

        private NewsFeedViewHolder(@NonNull View itemView) {
            super(itemView);
            labelTitle = itemView.findViewById(R.id.label_title);
            labelCategory = itemView.findViewById(R.id.label_category);
            imageView = itemView.findViewById(R.id.image_view);
            saveDeleteNews = itemView.findViewById(R.id.saving_delete_news);
            saveInPinList = itemView.findViewById(R.id.image_view_pin);
            containerView = itemView.findViewById(R.id.container);
        }

        @Override
        protected void bind(final int position) {
            final Results results = data.get(position);
            if (results != null) {
                if (NewsFeedManager.getInstance().getListType().equals(SAVED_LIST)) {
                    saveDeleteNews.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_delete_black));
                    saveInPinList.setVisibility(View.GONE);
                } else {
                    saveDeleteNews.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_save));
                    saveInPinList.setVisibility(View.VISIBLE);
                }

                labelTitle.setText(results.getWebTitle());
                //category field is not found
                labelCategory.setText(results.getType());
                if (results.getFields() != null) {
                    Picasso.get().load(results.getFields().getThumbnail()).into(imageView);
                }
            }

            containerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(v, position, results);
                }
            });

            saveDeleteNews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (results != null) {
                        results.setNewsList(true);
                        onClickListener.changeSavedList(results, NEWS_LIST);
                    }
                }
            });

            saveInPinList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (results != null) {
                        results.setPinList(true);
                        onClickListener.changeSavedList(results, PIN_LIST);
                    }
                }
            });
        }
    }
}
