package com.newsfeed.newsfeed.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.newsfeed.newsfeed.R;
import com.newsfeed.newsfeed.interfaces.OnRecyclerViewItemClickListener;
import com.newsfeed.newsfeed.model.Results;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.newsfeed.newsfeed.utils.Constants.NewsInfo.PIN_LIST;

public class PinnedAdapter extends RecyclerView.Adapter<PinnedAdapter.MyViewHolder> {
    private List<Results> data;
    private final OnRecyclerViewItemClickListener onClickListener;
    private final LayoutInflater mInflater;

    public PinnedAdapter(Context context, List<Results> data, OnRecyclerViewItemClickListener onClickListener) {
        this.data = data;
        this.onClickListener = onClickListener;
        this.mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new PinnedViewHolder(mInflater.inflate(R.layout.pinned_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Results> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public abstract class MyViewHolder extends RecyclerView.ViewHolder {

        private MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        protected abstract void bind(int position);
    }

    protected class PinnedViewHolder extends MyViewHolder {
        private ImageView imageViewPinItem;
        private ImageView deletePin;

        private PinnedViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewPinItem = itemView.findViewById(R.id.image_view_pin_item);
            deletePin = itemView.findViewById(R.id.pin_delete);
        }

        @Override
        protected void bind(final int position) {
            final Results results = data.get(position);
            if (results.getFields() != null) {
                Picasso.get().load(results.getFields().getThumbnail()).into(imageViewPinItem);
            }

            deletePin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    results.setNewsList(true);
                    onClickListener.deletePin(results, PIN_LIST);
                }
            });
        }
    }
}
