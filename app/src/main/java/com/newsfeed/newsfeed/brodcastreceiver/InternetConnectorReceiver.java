package com.newsfeed.newsfeed.brodcastreceiver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.newsfeed.newsfeed.activity.MainActivity;
import com.newsfeed.newsfeed.application.MyApplication;

public class InternetConnectorReceiver extends BroadcastReceiver {
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            boolean isVisible = MyApplication.isActivityVisible();
            if (isVisible) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (connectivityManager != null) {
                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                    if (networkInfo != null && networkInfo.isConnected()) {
                        new MainActivity().setConnected(true);
                    } else {
                        new MainActivity().setConnected(false);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}