package com.newsfeed.newsfeed.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;
import com.newsfeed.newsfeed.R;
import com.newsfeed.newsfeed.activity.ItemViewActivity;
import com.newsfeed.newsfeed.client.APIClient;
import com.newsfeed.newsfeed.interfaces.APIInterface;
import com.newsfeed.newsfeed.model.NewsFeed;
import com.newsfeed.newsfeed.model.Results;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

import static com.newsfeed.newsfeed.activity.BaseActivity.LOG_TAG;
import static com.newsfeed.newsfeed.utils.Constants.Notification.CHANNEL_ID;
import static com.newsfeed.newsfeed.utils.Constants.RefreshTime.REFRESH_DATA_TIME;
import static com.newsfeed.newsfeed.utils.Constants.RequestInfo.API_KEY;

public class ServiceManager extends Service {

    private static final int NOTIFICATION_ID = 1;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        final Handler h = new Handler();
        h.post(new Runnable() {
            @Override
            public void run() {
                sendRequest();
                h.postDelayed(this, REFRESH_DATA_TIME);
            }
        });
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    private void sendRequest() {
        String fromDate = String.format("%s", DateFormat.format("yyyy-MM-dd", System.currentTimeMillis()));
        final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        apiInterface.getNews(API_KEY, fromDate).enqueue(new Callback<NewsFeed>() {
            @Override
            public void onResponse(@NonNull Call<NewsFeed> call, @NonNull Response<NewsFeed> response) {
                final NewsFeed newsFeed = response.body();

                if (newsFeed == null || newsFeed.getResponse() == null) {
                    return;
                }

                List<Results> newList = newsFeed.getResponse().getResults();
                if (newList != null && newList.size() > 0) {
                    for (Results result : newList) {
                        compareDates(result);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<NewsFeed> call, @NonNull Throwable t) {
                Log.e(LOG_TAG, "Exception: %s", t.getCause());
            }
        });
    }

    private void compareDates(Results result) {
        try {
            String time = result.getWebPublicationDate();
            String hour = time.substring(11, 13);
            String minute = time.substring(14, 16);
            String second = time.substring(17, 19);

            int hourValue = Integer.parseInt(hour);
            int minuteValue = Integer.parseInt(minute);
            int secondValue = Integer.parseInt(second);

            String fromDate = String.format("%s", DateFormat.format("HH:mm:ss", System.currentTimeMillis()));
            String currentHour = fromDate.substring(0, 2);
            String currentMinute = fromDate.substring(3, 5);
            String currentSecond = fromDate.substring(6, 8);

            int currentHourValue = Integer.parseInt(currentHour);
            int currentMinuteValue = Integer.parseInt(currentMinute);
            int currentSecondValue = Integer.parseInt(currentSecond);

            if (currentHourValue * 60 + currentMinuteValue * 60 + currentSecondValue * 60 <=
                    hourValue * 60 + minuteValue * 60 + (secondValue + REFRESH_DATA_TIME / 1000) * 10) {
                createNotificationChannel(result);
            }
        } catch (Exception ignore) {
        }
    }

    private void createNotificationChannel(Results results) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            return;
        }

        Intent intent = ItemViewActivity.makeIntent(this, results);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, results.getWebTitle(), NotificationManager.IMPORTANCE_DEFAULT);
            if (notificationManager.getNotificationChannel(CHANNEL_ID) == null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.newsfeed_logo))
                .setSmallIcon(R.drawable.newsfeed_logo)
                .setContentTitle(results.getWebTitle())
                .setContentText(results.getType())
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
}
