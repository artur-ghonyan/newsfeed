package com.newsfeed.newsfeed.interfaces;

import com.newsfeed.newsfeed.model.NewsFeed;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("/search?q=12%20years%20a%20slave&format=json&tag=film/film,tone/reviews&show-tags=contributor&show-fields=starRating,headline,thumbnail,short-url&order-by=relevance")
    Call<NewsFeed> getNews(@Query("api-key") String apiKey, @Query("page") long page,
                           @Query("page-size") long pageSize, @Query("from-date") String fromDate);

    @GET("/search?format=json&tag=film/film,tone/reviews&show-tags=contributor&show-fields=starRating,headline,thumbnail,short-url&order-by=relevance")
    Call<NewsFeed> getNews(@Query("api-key") String apiKey, @Query("from-date") String fromDate);
}