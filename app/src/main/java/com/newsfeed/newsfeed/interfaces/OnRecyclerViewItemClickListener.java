package com.newsfeed.newsfeed.interfaces;

import android.view.View;
import com.newsfeed.newsfeed.model.Results;

public interface OnRecyclerViewItemClickListener {
    void onClick(View view, int position, Results results);

    void changeSavedList(Results results, String listType);

    void deletePin(Results results, String listType);
}
