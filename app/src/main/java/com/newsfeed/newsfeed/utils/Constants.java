package com.newsfeed.newsfeed.utils;

public interface Constants {

    interface NewsInfo {
        String TABLE_NAME = "newsInfo";

        String NEWS_TITLE = "newsTitle"; //select webTitle field
        String NEWS_IMG_URL = "newsImgUrl"; //select fields/thumbnail field
        String NEWS_CATEGORY = "newsCategory"; //select type field

        String NEWS_ID = "newsId";
        String NEWS_SECTION_ID = "sectionId";
        String NEWS_SECTION_NAME = "sectionName";
        String NEWS_WEB_PUBLICATION_DATE = "webPublicationDate";
        String NEWS_WEB_URL = "webUrl";
        String NEWS_API_URL = "apiUrl";
        String NEWS_FIELDS_HEAD_LINE = "headline";
        String NEWS_FIELDS_STAR_RATING = "starRating";
        String NEWS_FIELDS_SHORT_URL = "shortUrl";
        String NEWS_IS_HOSTED = "isHosted";
        String NEWS_PILLAR_ID = "pillarId";
        String NEWS_PILLAR_NAME = "pillarName";

        String NEWS_LIST = "newsList";
        String PIN_LIST = "pinList";
    }

    interface RequestInfo {
        String API_KEY = "17627a17-8aeb-4dd5-9ffc-3791409084d0";
        String FROM_DATE = "2010-01-01";
        int PAGE_SIZE = 10;
    }

    interface MenuItems {
        int LINEAR_LAYOUT_MANAGER = 101;
        int GRID_LAYOUT_MANAGER = 102;

        String NEWS_FEED_LIST = "newsFeedList";
        String SAVED_LIST = "savedList";
    }

    interface RefreshTime {
        Long REFRESH_DATA_TIME = 30 * 1000L;
    }

    interface Notification {
        String CHANNEL_ID = "channelId";
    }
}