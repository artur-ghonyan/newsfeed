package com.newsfeed.newsfeed.utils;

import static com.newsfeed.newsfeed.utils.Constants.MenuItems.NEWS_FEED_LIST;

public class NewsFeedManager {
    private static final NewsFeedManager ourInstance = new NewsFeedManager();
    private String listType = NEWS_FEED_LIST;

    public static NewsFeedManager getInstance() {
        return ourInstance;
    }

    private NewsFeedManager() {
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }
}
