package com.newsfeed.newsfeed.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.widget.Toast;
import com.newsfeed.newsfeed.database.NewsDBHelper;
import com.newsfeed.newsfeed.model.Fields;
import com.newsfeed.newsfeed.model.Results;

import java.util.ArrayList;
import java.util.List;

import static com.newsfeed.newsfeed.utils.Constants.NewsInfo.NEWS_LIST;

public class DBManager {
    private static final DBManager ourInstance = new DBManager();

    private NewsDBHelper newsDBHelper;
    private SQLiteDatabase sqLiteDatabase;

    public static DBManager getInstance() {
        return ourInstance;
    }

    private DBManager() {
    }

    public void checkNewsInDB(Context context, final Results results, String listType) {
        if (!TextUtils.isEmpty(results.getId())) {
            newsDBHelper = new NewsDBHelper(context);
            sqLiteDatabase = newsDBHelper.getReadableDatabase();
            Cursor cursor = newsDBHelper.searchNews(results.getId(), sqLiteDatabase);
            if (!cursor.moveToFirst()) {
                addNews(context, results);
                Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
            } else {
                if (cursor.moveToFirst()) {
                    do {
                        if (listType.equals(NEWS_LIST)) {
                            results.setPinList((cursor.getInt(1) == 1));
                        } else {
                            results.setNewsList((cursor.getInt(0) == 1));
                        }
                    } while (cursor.moveToNext());
                }
                updateNews(context, results);
                Toast.makeText(context, "Updates", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void addNews(Context context, Results results) {
        newsDBHelper = new NewsDBHelper(context);
        sqLiteDatabase = newsDBHelper.getWritableDatabase();
        newsDBHelper.addNews(results, sqLiteDatabase);
        newsDBHelper.close();
    }

    private void updateNews(Context context, Results results) {
        newsDBHelper = new NewsDBHelper(context);
        sqLiteDatabase = newsDBHelper.getWritableDatabase();
        newsDBHelper.updateNews(results, sqLiteDatabase);
        newsDBHelper.close();
    }

    public List<Results> getSavedResults(Context context) {
        List<Results> savedData = new ArrayList<>();
        newsDBHelper = new NewsDBHelper(context);
        sqLiteDatabase = newsDBHelper.getReadableDatabase();
        Cursor cursor = newsDBHelper.readInformation(sqLiteDatabase);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Results results = new Results();
                Fields fields = new Fields();

                results.setId(cursor.getString(0));
                results.setWebTitle(cursor.getString(1));
                results.setType(cursor.getString(3));
                results.setSectionId(cursor.getString(4));
                results.setSectionName(cursor.getString(5));
                results.setWebPublicationDate(cursor.getString(6));
                results.setWebUrl(cursor.getString(7));
                results.setApiUrl(cursor.getString(8));
                results.setHosted((cursor.getInt(12) == 1));
                results.setPillarId(cursor.getString(13));
                results.setPillarName(cursor.getString(14));

                fields.setThumbnail(cursor.getString(2));
                fields.setHeadline(cursor.getString(9));
                fields.setStarRating(cursor.getString(10));
                fields.setShortUrl(cursor.getString(11));
                results.setFields(fields);

                results.setNewsList((cursor.getInt(15) == 1));
                results.setPinList((cursor.getInt(16) == 1));

                savedData.add(results);
            } while (cursor.moveToNext());
        }
        return savedData;
    }

    public void delete(Context context, Results results, String listType) {
        if (!TextUtils.isEmpty(results.getId())) {
            newsDBHelper = new NewsDBHelper(context);
            sqLiteDatabase = newsDBHelper.getReadableDatabase();
            Cursor cursor = newsDBHelper.searchNews(results.getId(), sqLiteDatabase);
            if (cursor.moveToFirst()) {
                if (cursor.moveToFirst()) {
                    do {
                        if (listType.equals(NEWS_LIST)) {
                            if ((cursor.getInt(1) == 1)) {
                                results.setNewsList(false);
                                updateNews(context, results);
                            } else {
                                sqLiteDatabase = newsDBHelper.getWritableDatabase();
                                newsDBHelper.deleteNews(results.getId(), sqLiteDatabase);
                            }
                        } else {
                            if ((cursor.getInt(0) == 1)) {
                                results.setPinList(false);
                                updateNews(context, results);
                            } else {
                                sqLiteDatabase = newsDBHelper.getWritableDatabase();
                                newsDBHelper.deleteNews(results.getId(), sqLiteDatabase);

                            }
                        }
                    } while (cursor.moveToNext());
                }
            }
        }
    }
}
