package com.newsfeed.newsfeed.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static com.newsfeed.newsfeed.utils.Constants.MenuItems.LINEAR_LAYOUT_MANAGER;

public class PrefManager {
    private final SharedPreferences mPreference;
    private final SharedPreferences.Editor mEditor;

    private static final String SHARED = "news_feed";
    private static final String CLEAR = "clear";
    private static PrefManager INSTANCE;

    private static final String LAYOUT_MANAGER_TYPE = "layout_manager_type";

    private PrefManager(Context context) {
        mPreference = context.getSharedPreferences(SHARED, Context.MODE_PRIVATE);
        mEditor = mPreference.edit();
        if (mPreference.getBoolean(CLEAR, true)) {
            mEditor.clear();
            mEditor.putBoolean(CLEAR, false);
            mEditor.apply();
        }
    }

    public static PrefManager getInstance() {
        if (INSTANCE == null) {
            throw new IllegalStateException("PrefManager is not initialized");
        }
        return INSTANCE;
    }

    public static synchronized void init(Context context) {
        INSTANCE = new PrefManager(context);
    }

    public int getLayoutManagerType() {
        return mPreference.getInt(LAYOUT_MANAGER_TYPE, LINEAR_LAYOUT_MANAGER);
    }

    public void setLayoutManagerType(int layoutManagerType) {
        mEditor.putInt(LAYOUT_MANAGER_TYPE, layoutManagerType);
        mEditor.commit();
    }
}
