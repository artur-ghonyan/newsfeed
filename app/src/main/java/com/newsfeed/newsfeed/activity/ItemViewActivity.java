package com.newsfeed.newsfeed.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.newsfeed.newsfeed.R;
import com.newsfeed.newsfeed.model.Fields;
import com.newsfeed.newsfeed.model.Results;
import com.squareup.picasso.Picasso;

import static com.newsfeed.newsfeed.utils.Constants.NewsInfo.*;

public class ItemViewActivity extends AppCompatActivity {

    public static Intent makeIntent(Context context, Results results) {
        Intent intent = new Intent(context, ItemViewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(NEWS_TITLE, results.getWebTitle());
        intent.putExtra(NEWS_IMG_URL, results.getFields().getThumbnail());
        intent.putExtra(NEWS_CATEGORY, results.getType());
        intent.putExtra(NEWS_ID, results.getId());
        intent.putExtra(NEWS_SECTION_ID, results.getSectionId());
        intent.putExtra(NEWS_SECTION_NAME, results.getSectionName());
        intent.putExtra(NEWS_WEB_PUBLICATION_DATE, results.getWebPublicationDate());
        intent.putExtra(NEWS_WEB_URL, results.getWebUrl());
        intent.putExtra(NEWS_API_URL, results.getApiUrl());
        intent.putExtra(NEWS_FIELDS_HEAD_LINE, results.getFields().getHeadline());
        intent.putExtra(NEWS_FIELDS_STAR_RATING, results.getFields().getStarRating());
        intent.putExtra(NEWS_FIELDS_SHORT_URL, results.getFields().getShortUrl());
        intent.putExtra(NEWS_IS_HOSTED, results.getHosted());
        intent.putExtra(NEWS_PILLAR_ID, results.getPillarId());
        intent.putExtra(NEWS_PILLAR_NAME, results.getPillarName());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_view);
        init();
    }

    private void init() {
        final ImageView imageView = findViewById(R.id.image_view_event);
        final TextView title = findViewById(R.id.title_view_event);
        final TextView category = findViewById(R.id.category_view_event);
        final TextView id = findViewById(R.id.id_view_event);
        final TextView sectionId = findViewById(R.id.section_id_view_event);
        final TextView sectionName = findViewById(R.id.section_name_view_event);
        final TextView webPublicationDate = findViewById(R.id.web_publication_date_view_event);
        final TextView webUrl = findViewById(R.id.web_url_view_event);
        final TextView apiUrl = findViewById(R.id.api_url_view_event);
        final TextView headLine = findViewById(R.id.head_line_view_event);
        final TextView starRating = findViewById(R.id.star_rating_view_event);
        final TextView shortUrl = findViewById(R.id.short_url_view_event);
        final TextView isHosted = findViewById(R.id.hosted_view_event);
        final TextView pillarId = findViewById(R.id.pillar_id_view_event);
        final TextView pillarName = findViewById(R.id.pillar_name_view_event);

        Results results = getResult();
        title.setText(results.getWebTitle());
        category.setText(results.getType());
        id.setText(results.getId());
        sectionId.setText(results.getSectionId());
        sectionName.setText(results.getSectionName());
        webPublicationDate.setText(results.getWebPublicationDate());
        webUrl.setText(results.getWebUrl());
        apiUrl.setText(results.getApiUrl());
        headLine.setText(results.getFields().getHeadline());
        starRating.setText(results.getFields().getStarRating());
        shortUrl.setText(results.getFields().getShortUrl());
        isHosted.setText(String.format("%s", results.getHosted()));
        pillarId.setText(results.getPillarId());
        pillarName.setText(results.getPillarName());

        Picasso.get().load(results.getFields().getThumbnail()).into(imageView);
    }

    private Results getResult() {
        Intent intent = getIntent();
        final String title = intent.getStringExtra(NEWS_TITLE);
        final String url = intent.getStringExtra(NEWS_IMG_URL);
        final String category = intent.getStringExtra(NEWS_CATEGORY);
        final String id = intent.getStringExtra(NEWS_ID);
        final String sectionId = intent.getStringExtra(NEWS_SECTION_ID);
        final String sectionName = intent.getStringExtra(NEWS_SECTION_NAME);
        final String webPublicationDate = intent.getStringExtra(NEWS_WEB_PUBLICATION_DATE);
        final String webUrl = intent.getStringExtra(NEWS_WEB_URL);
        final String apiUrl = intent.getStringExtra(NEWS_API_URL);
        final String headLine = intent.getStringExtra(NEWS_FIELDS_HEAD_LINE);
        final String starRating = intent.getStringExtra(NEWS_FIELDS_STAR_RATING);
        final String shortUrl = intent.getStringExtra(NEWS_FIELDS_SHORT_URL);
        final boolean isHosted = intent.getBooleanExtra(NEWS_IS_HOSTED, false);
        final String pillarId = intent.getStringExtra(NEWS_PILLAR_ID);
        final String pillarName = intent.getStringExtra(NEWS_PILLAR_NAME);

        Results results = new Results();
        Fields fields = new Fields();

        results.setId(id);
        results.setWebTitle(title);
        results.setType(category);
        results.setSectionId(sectionId);
        results.setSectionName(sectionName);
        results.setWebPublicationDate(webPublicationDate);
        results.setWebUrl(webUrl);
        results.setApiUrl(apiUrl);
        results.setHosted(isHosted);
        results.setPillarId(pillarId);
        results.setPillarName(pillarName);

        fields.setThumbnail(url);
        fields.setHeadline(headLine);
        fields.setStarRating(starRating);
        fields.setShortUrl(shortUrl);
        results.setFields(fields);
        return results;
    }
}
