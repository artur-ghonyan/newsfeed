package com.newsfeed.newsfeed.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.newsfeed.newsfeed.application.MyApplication;
import com.newsfeed.newsfeed.service.ServiceManager;

public class BaseActivity extends AppCompatActivity {
    public static final String LOG_TAG = "MyLog";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stopServiceManager();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        startServiceManager();
    }

    private void startServiceManager() {
        startService(new Intent(this, ServiceManager.class));
    }

    private void stopServiceManager() {
        stopService(new Intent(this, ServiceManager.class));
    }
}
