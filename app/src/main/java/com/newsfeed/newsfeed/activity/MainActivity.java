package com.newsfeed.newsfeed.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import com.newsfeed.newsfeed.R;
import com.newsfeed.newsfeed.adapter.NewsFeedAdapter;
import com.newsfeed.newsfeed.adapter.PinnedAdapter;
import com.newsfeed.newsfeed.client.APIClient;
import com.newsfeed.newsfeed.interfaces.APIInterface;
import com.newsfeed.newsfeed.interfaces.OnRecyclerViewItemClickListener;
import com.newsfeed.newsfeed.model.NewsFeed;
import com.newsfeed.newsfeed.model.Results;
import com.newsfeed.newsfeed.utils.DBManager;
import com.newsfeed.newsfeed.utils.NewsFeedManager;
import com.newsfeed.newsfeed.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.newsfeed.newsfeed.utils.Constants.MenuItems.*;
import static com.newsfeed.newsfeed.utils.Constants.NewsInfo.PIN_LIST;
import static com.newsfeed.newsfeed.utils.Constants.RefreshTime.REFRESH_DATA_TIME;
import static com.newsfeed.newsfeed.utils.Constants.RequestInfo.*;

public class MainActivity extends BaseActivity implements OnRecyclerViewItemClickListener {
    private RecyclerView newsFeedRecyclerView;
    private RecyclerView pinnedRecyclerView;
    private NewsFeedAdapter newsFeedAdapter;
    private PinnedAdapter pinnedAdapter;
    private Menu menu;
    private boolean isConnected;
    private long currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        List<Results> newsList = new ArrayList<>();
        newsFeedRecyclerView = findViewById(R.id.recycler_view_vertical);
        pinnedRecyclerView = findViewById(R.id.recycler_view_horizontal);
        updateLayoutManager();
        newsFeedAdapter = new NewsFeedAdapter(this, newsList, this);
        newsFeedRecyclerView.setAdapter(newsFeedAdapter);

        List<Results> pinList = getPinList();
        pinnedAdapter = new PinnedAdapter(this, getPinList(), this);
        pinnedRecyclerView.setAdapter(pinnedAdapter);
        if (pinList != null && pinList.size() > 0) {
            pinnedRecyclerView.setVisibility(View.VISIBLE);
        } else {
            pinnedRecyclerView.setVisibility(View.GONE);
        }

        newsFeedRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                // Check if not displaying saved items
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                    int lastVisibleItemPosition;
                    if (layoutManager instanceof LinearLayoutManager) {
                        lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                    } else {
                        lastVisibleItemPosition = ((StaggeredGridLayoutManager) layoutManager).findLastVisibleItemPositions(null)[1];
                    }

                    if (lastVisibleItemPosition == newsFeedAdapter.getItemCount() - 1) {
                        // Request new page
                        requestNewPage();
                    }
                }
            }
        });
        getNewsList();
        checkInternetConnect();
    }

    private void requestNewPage() {
        final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        apiInterface.getNews(API_KEY, currentPage + 1, PAGE_SIZE, FROM_DATE).enqueue(new Callback<NewsFeed>() {
            @Override
            public void onResponse(@NonNull Call<NewsFeed> call, @NonNull Response<NewsFeed> response) {
                final NewsFeed newsFeed = response.body();
                if (newsFeed == null) {
                    return;
                }

                currentPage = newsFeed.getResponse().getCurrentPage();

                if (newsFeed.getResponse() != null && NewsFeedManager.getInstance().getListType().equals(NEWS_FEED_LIST)) {
                    List<Results> newList = newsFeed.getResponse().getResults();
                    List<Results> oldList = newsFeedAdapter.getData();
                    if (oldList.size() != newList.size() || !oldList.containsAll(newList)) {
                        newList.addAll(0, oldList);
                        newsFeedAdapter.setData(newList);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<NewsFeed> call, @NonNull Throwable t) {
                Log.e(LOG_TAG, "Exception: %s", t.getCause());
            }
        });
    }

    private List<Results> getPinList() {
        List<Results> results = DBManager.getInstance().getSavedResults(this);
        List<Results> pinList = new ArrayList<>();
        for (Results result : results) {
            if (result.isPinList()) {
                pinList.add(result);
            }
        }
        Collections.reverse(pinList);
        results.clear();
        return pinList;
    }

    private void checkInternetConnect() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                setConnected(true);
            } else {
                setConnected(false);
            }
        }
    }

    private boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean isConnected) {
        if (!this.isConnected && isConnected) {
            getNewsList();
        }
        this.isConnected = isConnected;
    }

    private void updateLayoutManager() {
        RecyclerView.LayoutManager layoutManager;
        if (PrefManager.getInstance().getLayoutManagerType() == LINEAR_LAYOUT_MANAGER) {
            layoutManager = new LinearLayoutManager(this);
        } else {
            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        }
        newsFeedRecyclerView.setLayoutManager(layoutManager);
    }

    private void getNewsList() {
        final Handler h = new Handler();
        h.post(new Runnable() {
            @Override
            public void run() {
                checkInternetConnect();
                if (isConnected()) {
                    sendRequest();
                } else {
                    Log.e(LOG_TAG, "no internet connection");
                }
                h.postDelayed(this, REFRESH_DATA_TIME);
            }
        });
    }

    private void sendRequest() {
        final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        apiInterface.getNews(API_KEY, currentPage, currentPage * PAGE_SIZE, FROM_DATE).enqueue(new Callback<NewsFeed>() {
            @Override
            public void onResponse(@NonNull Call<NewsFeed> call, @NonNull Response<NewsFeed> response) {
                final NewsFeed newsFeed = response.body();

                if (newsFeed == null) {
                    return;
                }

                currentPage = newsFeed.getResponse().getCurrentPage();

                if (newsFeed.getResponse() != null && NewsFeedManager.getInstance().getListType().equals(NEWS_FEED_LIST)) {
                    List<Results> newList = newsFeed.getResponse().getResults();
                    List<Results> oldList = newsFeedAdapter.getData();
                    if (!oldList.containsAll(newList)) {
                        newsFeedAdapter.setData(newList);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<NewsFeed> call, @NonNull Throwable t) {
                Log.e(LOG_TAG, "Exception: %s", t.getCause());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        menuLayoutItemsVisible();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.saved_list:
                NewsFeedManager.getInstance().setListType(SAVED_LIST);
                menuListItemsVisible();
                List<Results> results = DBManager.getInstance().getSavedResults(MainActivity.this);
                List<Results> newsResults = new ArrayList<>();
                for (Results result : results) {
                    if (result.isNewsList()) {
                        newsResults.add(result);
                    }
                }
                results.clear();
                newsFeedAdapter.setData(newsResults);
                break;
            case R.id.news_feed_list:
                NewsFeedManager.getInstance().setListType(NEWS_FEED_LIST);
                menuListItemsVisible();
                getNewsList();
                break;
            case R.id.linear_layout:
                setLayoutManagerType(GRID_LAYOUT_MANAGER);
                break;
            case R.id.grid_layout:
                setLayoutManagerType(LINEAR_LAYOUT_MANAGER);
                break;
            default:
                break;
        }
        return true;
    }

    private void setLayoutManagerType(int type) {
        PrefManager.getInstance().setLayoutManagerType(type);
        updateLayoutManager();
        menuLayoutItemsVisible();
        if (newsFeedAdapter != null) {
            newsFeedAdapter.notifyDataSetChanged();
        }
    }

    private void menuListItemsVisible() {
        if (NewsFeedManager.getInstance().getListType().equals(SAVED_LIST)) {
            menu.findItem(R.id.news_feed_list).setVisible(true);
            menu.findItem(R.id.saved_list).setVisible(false);
        } else {
            menu.findItem(R.id.news_feed_list).setVisible(false);
            menu.findItem(R.id.saved_list).setVisible(true);
        }
    }

    private void menuLayoutItemsVisible() {
        if (PrefManager.getInstance().getLayoutManagerType() == LINEAR_LAYOUT_MANAGER) {
            menu.findItem(R.id.linear_layout).setVisible(true);
            menu.findItem(R.id.grid_layout).setVisible(false);
        } else {
            menu.findItem(R.id.linear_layout).setVisible(false);
            menu.findItem(R.id.grid_layout).setVisible(true);
        }
    }

    @Override
    public void onClick(View view, int position, Results results) {
        if (view.getId() == R.id.container) {
            Intent intent = ItemViewActivity.makeIntent(this, results);
            startActivity(intent);
        }
    }

    @Override
    public void changeSavedList(Results results, String listType) {
        if (NewsFeedManager.getInstance().getListType().equals(NEWS_FEED_LIST)) {
            DBManager.getInstance().checkNewsInDB(this, results, listType);
            if (listType.equals(PIN_LIST) && pinnedAdapter != null) {
                if (pinnedRecyclerView.getVisibility() != View.VISIBLE) {
                    pinnedRecyclerView.setVisibility(View.VISIBLE);
                }
                pinnedAdapter.setData(getPinList());
            }
        } else {
            DBManager.getInstance().delete(this, results, listType);
            NewsFeedManager.getInstance().setListType(SAVED_LIST);
            menuListItemsVisible();
            List<Results> results1 = DBManager.getInstance().getSavedResults(MainActivity.this);
            List<Results> newsResults = new ArrayList<>();
            for (Results result : results1) {
                if (result.isNewsList()) {
                    newsResults.add(result);
                }
            }
            results1.clear();
            newsFeedAdapter.setData(newsResults);
        }
    }

    @Override
    public void deletePin(Results results, String listType) {
        DBManager.getInstance().delete(this, results, listType);
        pinnedAdapter.setData(getPinList());
    }
}